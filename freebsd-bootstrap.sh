#!/bin/sh

# Update Freebsd to the latest release
echo Updating freebsd to latest release..
freebsd-update fetch install

# Extract ports
echo Extract ports..
portsnap fetch extract

# install portmaster
echo Install portmaster..
cd /usr/ports/ports-mgmt/portmaster/
make install clean

# install apps
echo Install apps..
cd /usr/ports
portmaster editors/vim editors/emacs devel/mercurial devel/subversion shells/bash shells/zsh www/opera www/lynx ftp/wget ftp/curl x11/xorg devel/apache-ant devel/maven3 devel/gradle java/openjdk8 security/sudo

# checkout freebsd source before installing virtualbox additions
echo Checkout freebsd source..
svn checkout https://svnweb.freebsd.org/base/releng/10.2/ /usr/src

# install virtualbox additions
echo Install Virtualbox additions..
portmaster emulators/virtualbox-ose-additions

echo Cleaning ports
portmaster --clean-distfiles -y

echo Freebsd post-setup completed!
